package books;


public class TitleNouns {
	public static void loadNouns(){
		new Noun("Immortal", 2, 1, 1, 1);
		new Noun("Pirate", 2, 1, 1, 1);
		new Noun("Unicorn", 2, 1, 1, 1);
		new Noun("Dog", 2, 1, 1, 1);
		new Noun("War", 1, 1, 1, 1);
		new Noun("World", 2, 1, 1, 1);
		new Noun("Guardian", 2, 1, 1, 1);
		new Noun("Watcher", 2, 1, 1, 1);
		new Noun("Revenge", 1, 1, 0, 0);
		new Noun("Vengeance", 1, 1, 0, 0);
		new Noun("Secret", 2, 1, 1, 0);
		new Noun("Star", 2, 1, 1, 0);
		new Noun("Robot", 2, 1, 1, 1);
		new Noun("Flight", 3, 1, 1, 0);
		new Noun("Ship", 2, 1, 1, 0);
		new Noun("Man", "Men", 2, 1, 1, 1);
		new Noun("Warrior", 2, 1, 1, 1);
		new Noun("Curse", 0, 1, 1, 0);
		new Noun("Return", 0, 1, 0, 0);
		new Noun("Galaxy", "Galaxies", 2, 1, 1, 0);
		new Noun("Spy", "Spies", 2, 1, 1, 0);
//		new Noun("Hidden", "Hidden", 0, 1, 1, 0);
		new Noun("Law", 0, 1, 0, 0);
		new Noun("Bomb", 0, 1, 1, 0);
		new Noun("Fleet", 2, 1, 1, 1);
//		new Noun("Wild", 0, 0, 0, 0);
		new Noun("Kingdom", 2, 1, 1, 1);
		new Noun("Snake", 2, 1, 1, 1);
		new Noun("Shadow", 2, 1, 1, 0);
		new Noun("Knight", 2, 1, 1, 1);
		new Noun("Nation", 2, 1, 1, 1);
		new Noun("State", 2, 1, 1, 0);
		new Noun("Federation", 0, 1, 1, 0);
		new Noun("Game", 0, 1, 0, 0);
		new Noun("Doom", 1, 1, 0, 1);
		new Noun("Death", 1, 1, 0, 1);
		new Noun("End", 0, 1, 0, 1);
		new Noun("Hammer", 0, 1, 1, 1);
		new Noun("Sword", 0, 1, 1, 1);
		new Noun("Dragon", 0, 1, 1, 1);
		new Noun("Beast", 0, 1, 1, 1);
		new Noun("Gate", 0, 1, 1, 1);
		new Noun("Ghost", 0, 1, 1, 1);
		new Noun("Destroyer", 0, 1, 1, 0);
		new Noun("Destruction", 1, 1, 0, 1);
		new Noun("Fate", 1, 0, 0, 1);
		new Noun("Hope", 1, 0, 1, 1);
		new Noun("Chance", 1, 0, 1, 1);
		new Noun("Nightmare", 2, 1, 1, 1);
		new Noun("Day", 2, 1, 1, 1);
		new Noun("Time", 3, 1, 1, 1);
		new Noun("Agent", 2, 1, 1, 0);
		new Noun("Theorem", 2, 1, 1, 0);
		new Noun("Formula", 2, 1, 1, 0);
	}
}
