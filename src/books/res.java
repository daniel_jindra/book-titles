package books;

///// Grammatical, Mathematical, Logical, etc Resources - Raz

public class res {
	public static String getRanName (Gender g){
		String [] mlist = {"Ender", "John", "Kyle", "Danny", "Alex", "Adam", "Zack", "Toby", "Domenic", "Jonas", "Long", "Ken"};
		String [] flist = {"Carissa", "Chau", "Clemencia", "Lisa", "Susan", "Shandra", "Skye", "Alyx", "Candie", "Jesusita", "Leora"};
		switch(g){
		case m:
			return mlist[randNum(mlist.length)];
		case f:
			return flist[randNum(flist.length)];
		}
		return "error";
	}
	
	public static boolean random(){
		return Math.random() < 0.5;
	}
	
	public static int randNum(int max){
		return (int) (max * Math.random());
	}
	public static int randNum(int min, int max){
		return (int) (min + (max - min) * Math.random());
	}
	
	public static String lower(String s){
		String temp = s.toLowerCase();
		StringBuilder ret = new StringBuilder();
		ret.append(temp.charAt(0));
		ret.append(s.substring(1));
		
		return ret.toString();
	}
	
	public static String upper(String s){
		String temp = s.toUpperCase();
		StringBuilder ret = new StringBuilder();
		ret.append(temp.charAt(0));
		ret.append(s.substring(1));
		
		return ret.toString();
	}
	
//	public static String heshe(Gender g){
//		switch(g){
//		case m:
//			return "he";
//		case f:
//			return "she";
//		case h:
//			return "shi";
//		}
//		return "error";
//	}
	public static String hisher(Gender g){
		switch(g){
		case m:
			return "his";
		case f:
			return "her";
		}
		return "error";
	}
//	public static String himher(Gender g){
//		switch(g){
//		case m:
//			return "him";
//		case f:
//			return "her";
//		case h:
//			return "hir";
//		}
//		return "error";
//	}
	
	public static int transNum (String s){
		int ret = 0;
		for (int i = 0; i < s.length(); i++){
//			System.out.println(s.charAt(i));
			if(s.charAt(i) < 48 || s.charAt(i) > 58){
				return -1;
			}
			ret += ((s.charAt(i) - 48) * Math.pow(10, (s.length() - i -1)));
		}
//		System.out.println("You entered: " + s + ", I understood: " + ret);
		return ret;
	}
	public static String randHotelName(){
		String[] list = {"CureHotel","DudeSuite","GuestPro","GuestUnits","LeadHotel","HotelStuds","RestUSA","WiseStay","AllFrills","HolidayInn"};
		return list[randNum(list.length)];
	}
	
	public static boolean bool (int n){
		if (n <= 0){
			return false;
		} else {
			return true;
		}
	}
	
	public static String intToCount(int n){
		switch(n){
		case 0: return "";
		case 1:	return "First";
		case 2:	return "Second";
		case 3:	return "Third";
		case 4: return "Fourth";
		case 5: return "Fifth";
		case 6: return "Sixth";
		case 7: return "Seventh";
		case 8: return "Eighth";
		case 9: return "Ninth";
		default: return n +"th";
		}
	}
}
