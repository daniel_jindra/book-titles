package books;


public enum Gender {
	m{
	    public String toString() {
	        return "male";
	    }    
	}, f{
	    public String toString() {
	        return "female";
	    }
	};
	
	public static Gender getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }

}