package books;

public class Noun {
	String word = "";
	String plural = "";
	String possessive = "";
	int worksWithOf = 0;
	boolean isCountable = false;
	boolean hasPossessive = false;
	boolean hasPlural = false;
	
	Noun (String st, int of, int count, int plu, int pos){
		word = st;
		
		worksWithOf = of;
		isCountable = res.bool(count);
		if (res.bool(plu)){
			plural = st + "s";
			hasPlural = true;
		}
		if (res.bool(pos)){
			possessive = st+"'s";
			hasPossessive = true;
		}
		Titles.NounList.add(this);
	}
	
	Noun (String st, String st2, int of, int count, int plu, int pos){
		word = st;
		plural = st2;
		worksWithOf = of;
		isCountable = res.bool(count);
		if (plural != ""){
			hasPlural = true;
		}
		if (res.bool(pos)){
			possessive = st+"'s";
			hasPossessive = true;
		}
		Titles.NounList.add(this);
	}
	
	Noun (String st, String st2, String st3, int of, int count, int plu, int pos){
		word = st;
		plural = st2;
		possessive = st3;
		hasPossessive = true;
		worksWithOf = of;
		isCountable = res.bool(count);
		if (plural != ""){
			hasPlural = true;
		}
		Titles.NounList.add(this);
	}
	
	public String singOrPlu(){
		if (this.hasPlural){
			if(res.random()){
				return this.word;
			} else {
				return this.plural;
			}
		} else {
			return this.word;
		}
			
	}
	
}
