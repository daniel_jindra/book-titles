package books;

public enum TitleAdjectives {
	Dark, Wild, Secret, Cursed, Terrible, Dead, Red, Blue, Black, Green, Forgotten, Mechanical, Electrical, Cybernetic, Telepathic, Systematic, New, Old, Total, Broken, Bad, Last, Final, Deadly, Only ;
	
	
	public static String getRandom() {
        return values()[(int) (Math.random() * (values().length))].toString();
    }
	public static String getMaybe() {
	    if(Math.random() < 0.5){
	    	return values()[(int) (Math.random() * (values().length))].toString() + " ";
	    } else {
	    	return "";
	    }
    }
}
