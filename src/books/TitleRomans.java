package books;

public enum TitleRomans {
	I, II, III, IV, V, VI, VII, VIII, IX, X;
	
	public static String getRandom() {
		int num = (int) (Math.random() * (values().length + 2) -2);
		if(num > 0){
			return " " + values()[num].toString();
		} else {
			return "";
		}
    }
	public static String getOne() {
		int num = (int) (Math.random() * (values().length));
		return " " + values()[num].toString();
    }
}
