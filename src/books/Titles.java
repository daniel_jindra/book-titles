package books;

import java.util.ArrayList;
import java.util.List;
//import java.util.LinkedList;

public class Titles {
	public static List<Noun> NounList = new ArrayList<Noun>();
	public static String XoftheXtheX(){
		return (RandNoun() + " of the " + RandNoun() + TitleRomans.getRandom()  + " - The " + maybeCount() + TitleAdjectives.getMaybe() + RandNoun());
	}
//	
//	public static String theXofXX(){
//		return ("The " + RandNoun() + " of " + TitleNouns.getRandom() + TitleRomans.getRandom()  + ": " + TitleNouns.getRandom());
//	}
//	
//	public static String XX(){
//		return (TitleNounsNotOf.getRandom() + " " + RandNoun()+"!");
//	}
	
	public static String Title1(){
		
		boolean the = false;
		StringBuilder ret = new StringBuilder();
		/// The or not at the start
		if(res.random()){
			ret.append("The ");
			the = true;
		}
		
		/// X of the X or X's
		if(res.random()){
			
			///choose for really short
			if(res.randNum(100) < 65){
				ret.append(RandNoun() + " ");
			} else {
				/// really short
				if(res.random()){
					ret.append(SearchNoun(-1, -1, -1, 1) + " ");
				} else {
					ret.append(SearchNoun(-1, 1, 0, 0) + " ");
				}
				ret.append(RandNoun() + "!");
				return ret.toString();
					
			}
				
					
			
			
			/// "of" or "of the"
			if(res.random()){
				ret.append("of ");
				ret.append(SearchNoun(res.randNum(1, 3), -1, -1, -1));
			} else {
				ret.append("of the ");
				ret.append(RandNoun());
			}
		} else {
			//// Thing's or Char's
			if(the){
				ret.append(TitleAdjectives.getMaybe() + SearchNoun(-1, -1, -1, 1) + " " + maybeCount() + RandNoun());
			} else {
				Gender g = Gender.getRandom();
				ret.append(res.getRanName(g));
				/// X's or "and their"
				if (res.random()){
					ret.append("'s ");
				}else {
					ret.append(" and " + res.hisher(g) + " ");
				}
				/// Count or not
				if (res.random()){
					// Word order
					if(res.random()){
						ret.append(defCount() + TitleAdjectives.getMaybe() + SearchNoun(0, 1, 0, 0));
					} else {
						ret.append(TitleAdjectives.getMaybe() + defCount() + SearchNoun(0, 1, 0, 0));
					}
					
				} else {
					ret.append(TitleAdjectives.getMaybe() + RandNoun());
				}
			}
		}
		
		/// NUMBER
		if(res.random()){
			if(res.random()){
				if(res.random()){
					ret.append(" - Book ");
				} else {
					ret.append(" - Part ");
				}
			}
			ret.append(TitleRomans.getOne());
		}
		
		
		/// SUB TITLE
		if (res.random()){

			ret.append(": The ");
			/// Count or not
			if (res.random()){
				/// Word order
				if(res.random()){
					ret.append(maybeCount() + TitleAdjectives.getMaybe() + SearchNoun(0, 1, 0, 0));
				}else {
					ret.append(TitleAdjectives.getMaybe() + maybeCount() + SearchNoun(0, 1, 0, 0));
				}
				
			} else {
				ret.append(TitleAdjectives.getMaybe() + RandNoun());
			}
		}
		
		
		for (int i = 0; i < ret.length(); i++){
			if(ret.charAt(i) == ' ' && ret.charAt(i-1) == ' '){
				ret.deleteCharAt(i);
				i--;
			}
		}
		return ret.toString();
		
//		( + " of the " + RandNoun() + TitleRomans.getRandom()  + " - The " + maybeCount() + TitleAdjectives.getMaybe() + RandNoun());
	}
	
	private static String RandNoun(){
		return NounList.get(res.randNum(NounList.size())).singOrPlu();
	}
			
	private static String SearchNoun(int of, int count, int plu, int pos){
		int n = res.randNum(1, NounList.size());
		int u = 0;
		
		for(int i = 0; u <= n; i++){
//			System.out.println(u + " " + n + "    " + of + " " + count + " " + plu + " " + pos + " " + i);
			boolean moved = false;
			if (i == NounList.size()){
				i -= NounList.size();
			}
			
			if (res.bool(of)){
				
				if (NounList.get(i).worksWithOf != 0){
					u++;
					moved = true;
				}
			} 
			if (res.bool(count)){
				if (NounList.get(i).isCountable){
					if (!moved){
						u++;
						moved = true;
					}
				} else {
					moved = false;
				}
			}
			if (res.bool(plu)){
				if (NounList.get(i).hasPlural){
					if (!moved){
						u++;
						moved = true;
					}
				} else {
					moved = false;
				}
			}
			if (res.bool(pos)){
				if (NounList.get(i).hasPossessive){
					if (!moved){
						u++;
						moved = true;
					}
				} else {
					moved = false;
				}
			} 
			
			
			if ( u == n){
				if(res.bool(of)){
					switch (NounList.get(i).worksWithOf){
					case 1: return NounList.get(i).singOrPlu();
					case 2: return NounList.get(i).plural;
					case 3: return NounList.get(i).word;
					}
				} else if (res.bool(pos)){
					return NounList.get(i).possessive;
				} else if (res.bool(count)){
					return NounList.get(i).word;
				} else{
					return NounList.get(i).singOrPlu();
				}
			}
		}
		return "!!! ERROR " + of + " " + count + " " + plu + " " + pos + "!!!";
	}
	
	public static String defCount(){
		return (res.intToCount(res.randNum(13)) + " ");
	}
	
	public static String maybeCount(){
		if(res.random()){
			return (res.intToCount(res.randNum(13)) + " ");
		} else {
			return "";
		}
	}
	
	public static void main(String[] args){
		TitleNouns.loadNouns();
		
		for (int i = 0; i < 20; i++){
//			System.out.println(XoftheXtheX());
			String res = Title1();
			if (res.contains("The Only")){
				System.out.println(res);
			} else {
				i--;
			}
			
		}
		{
//			int times = 1000;
//			for (int x = 0; x < times/10; x++){
//				int chance = 0;
//				
//				for (int i = 0; i < times; i++){
//					if (res.random()){
////						System.out.println("ding");
//						chance ++;
//					}
//				}
//				System.out.println((double) chance/times);
//			}
		}
		
		
		
//		for (int i = 0; i < 10; i++){
//			System.out.println(theXofXX());
//		}
//		for (int i = 0; i < 10; i++){
//			System.out.println(XX());
//		}
	}
}